// Sentencianes que siempre te van a devolver un estado boleano (verdadero o falso)
// === igual a
// !== diferente a
// > mayor que
// < menor que
// >= mayor o igual que
// <= menor o igual que

var carro1 = {
  listoParaVenta: true
}

var carro2 = {
  listoParaVenta: false
}

if (carro2.listoParaVenta === true){
  console.log('Carro vendido');
} else { // si no
  console.log('El carro no esta listo para venta!!! :(');
}

var playaDelCarmen = {
  nombre: 'Playa del Carmen',
  numeroDeHabitantes: 304942,
  area: 120,
  unidades: 'km^2',
  ubicacion: 'Sur este de la republica'
}

var QuintanaRoo = {
  numeroDeHabitantes: 1857985,
  municipios: [playaDelCarmen.nombre],
  ubicacion: 'Sur este de la republica'
}

if(playaDelCarmen.ubicacion === QuintanaRoo.ubicacion) {
  console.log('Estan en el mismo lugar?');
  if(QuintanaRoo.municipios.includes('Playa del Carmen')){
    console.log('No! Pero playa del Carmen es un municipio de Quintana Roo');
  }
}

// && y Carmen y Jorge -> Carmen && Jorge
// || o Rodolfo o Jair -> Rodolfo || Jair

// if (condicional) {
//    Se ejecuta el bloque de codigo si la condicional es verdadera
// } else {
//    Se ejecuta el bloque de codigo si la condicional es falsa
// }

// if (condicional && condicional2) {
//    Se ejecuta el bloque de codigo si la condicional es verdadera
// }

// if (condicional || condicional2) {
//    Se ejecuta el bloque de codigo si la condicional es verdadera
// }

// if (condicional) {
//    Se ejecuta el bloque de codigo si la condicional es verdadera
// } else if(condicional) {
//    Se ejecuta el bloque de codigo si la condicional es verdadera
// } else {
//    Se ejecuta el bloque de codigo si las condicional son falsa
// }

var numero = 0;
var numero1 = 1;
var numero2 = 2;

if(numero1 > numero && numero < numero2) { // Si numero1 es mayor a numero y numero es menor a numero2
  console.log('Verdadero');
}

if(numero1 > numero || numero < numero2) { // Si numero1 es mayor a numero o numero es menor a numero2
  console.log('Verdadero');
} else {
  console.log('False');
}

// Ejercicio
// programar un if simple, un if else, un if con sentencias && y un if con sentencias ||


var n = 100;
var n1 = 150;
var n2 = 200;
var n3 = 250;
var n4 = 300;

if (n < n4 && n1 > n2){
    console.log("True");
} else {
    console.log("False");
} // False

if (n < n3 || n4 > n2){
    console.log("True");
} else {
    console.log("False");
} // Verdadero