var cadenaDeTexto = "Hola Mundo"; // Aqui declare una variable de tipo cadena de texto
var numero = 15; // Aqui declare una variable de tipo numero
var verdadero = true; // Aqui declare una variable de tipo buleano/Boolean
var falso = false; // Aqui declare una variable de tipo buleano/Boolean

var ejemploDeArreglo = [cadenaDeTexto, numero, verdadero, falso, 2.13]; // Aqui declare una variable de tipo Arreglo o Array
console.log(ejemploDeArreglo[0]);

var ejemploObjeto = {
  cajaConCadenaDeTexto: cadenaDeTexto,
  cajaConNumero: numero,
  cajaConVariableAleatoria: 'Lo que querramos'
};

console.log(ejemploObjeto.cajaConNumero);

// Ejercicio
// 1. Declarar una variable de tipo Arreglo que contenga los numeros del 1 al 10
var numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
console.log(numero[0]);
// 2. Declarar una variable de tipo Objeto que sea un gato
var gato = {
  numeroDePatas: 4,
  suSonidoCaracterizticoEsElMaullido: true,
  tieneBigotes: true,
  color: 'Blanco',
  vidas: 1,
  tieneCola: true
};

console.log('El gato tiene cola?', gato.tieneCola);
console.log('De que color es el gato?',gato.color);

var carro = {
  llantas: 4,
  modelo: "Prius'2 ",
  marca: 'Toyota',
  color: 'Rojo',
  precio: 150000,
  año: 2012,
  interesados: ['Rodolfo', 'Juanita', 'Perenganito']
};

console.log('Todo el carro', carro);
console.log('Precio carro?',carro.precio);
console.log('Color carro?', carro.color);
console.log('Todos los interesados?', carro.interesados);
console.log('Interesado en la primer posicion?', carro.interesados[0]);

// Declarar 2 objetos a su gusto
// Los objetos se tienen que conectar (uno tiene que ser hijo del otro) //Tip Objetos relacionales en JS