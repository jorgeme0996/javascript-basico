// formula para un ciclo for "for(inicializacion de variable contador; condición de ejecución; incremento)"
// variable ++ suma variable más uno
for(var i = 0; i <= 10000; i++) {
  // codigo que se ejecuta si a condición es
  console.log(i);
}

// variable += n suma varibale más n
for(var i = 0; i <= 10000; i+=5) {
  // codigo que se ejecuta si a condición es
  console.log(i);
}