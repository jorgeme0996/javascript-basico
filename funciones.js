//Una función es un fragmento de código qué contiene una acción especifíca, calculo de valor.
// la palabra dedicada "function"
// function+nombre de la función+(dentro de los paréntesis, lo que se le va a pasar a la función)+
// { dentro de las llaves la lógica de la función+return+lo que se vaya a retornar}

//función sin parametros de entrada y sin retorno de datos
function funcionSinParametrosEntrada () {
  //lo que quiero que haga
  console.log('Hola soy una función que no recibe nada.');
}

//función con parametros de entrada y sin retorno de datos
function funcionConParametrosEntrada (param1) {
  //lo que quiero que haga
  console.log('Hola, yo soy una función '+
      'que esta recibiendo a param1 y vale: ', param1);
}

//función que retorna datos
function funcionConRetornoDeDato () {
  //lo que quiero que haga
  return 1;
}

//funcionSinParametrosEntrada();
/*
funcionConParametrosEntrada(1);
funcionConParametrosEntrada('hola');
funcionConParametrosEntrada();*/


//console.log(funcionConRetornoDeDato());
//funcionConRetornoDeDato();

function suma(a, b){
  return a+b;
}

var valor1 = 5;
var valor2 = 10;

//console.log('Mi suma de ',valor1, ' más ', valor2, 'es: ', suma(valor1,valor2));

function saluda(nombre){
  return 'Hola '+nombre;
}

//console.log(saluda('Rodolfo'));

function suma2(a=1,b=1){
  return a+b;
}
//console.log(suma2(4,6));

function saludo2(nombre='juanito'){
  return 'Hola, mucho gusto '+ nombre;
}
console.log(saludo2('Jair'))
