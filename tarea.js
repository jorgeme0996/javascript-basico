var productos = [
  {
    nombre:'Fanta',
    precio: 25
  },
  {
    nombre: 'Coca-cola',
    precio: 75
  },
  {
    nombre: 'Agua',
    precio: 100
  }
]

function calculadoraDelIva(productos){
  var precioConIva = 0;
  var Iva = 0;
  var IvaEnMexico = 0.16;
  var total = 0;
  for(var i = 0; i < productos.length; i++){
    total=total+productos[i].precio
  }
  Iva = total*IvaEnMexico;
  precioConIva = total+Iva;
  return {total:precioConIva , IVa:Iva, subtotal:total};
}
var granTotal=calculadoraDelIva(productos);
console.log(granTotal);
