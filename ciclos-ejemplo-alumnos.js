var alumna1 = {
  nombre: 'Juanita Perez',
  calificacion: 5
}

var alumna2 = {
  nombre: 'Isabel Guzman',
  calificacion: 10
}

var alumna3 = {
  nombre: 'Rodolfo Martinez',
  calificacion: 9
}

var alumna4 = {
  nombre: 'Jorge Gomez',
  calificacion: 2
}

var alumnas = [alumna1, alumna2, alumna3, alumna4];
var alumnasAprobadas = [];
var alumnasReprobadas = [];

for(var i = 0; i < alumnas.length; i++) {
  console.log(alumnas[i]);
  if(alumnas[i].calificacion < 6) {
    console.log('La alumna ' + alumnas[i].nombre + ' REPROBO!!!!');
    alumnasReprobadas.push(alumnas[i]);
  } else {
    console.log('La alumna ' + alumnas[i].nombre + ' APROBO!!!!');
    alumnasAprobadas.push(alumnas[i]);
  }
}

console.log('Aprobadas: ', alumnasAprobadas);
console.log('Reprobadas: ', alumnasReprobadas);

// Ejercicio: programar un bucle que te diga si los vuelos de una aerolinea tienen asientos disponibles