var producto1 = {
  precio: 12,
  nombre: 'Panditas',
  stock: 0
}

var producto2 = {
  precio: 30,
  nombre: 'Crema Alpura',
  stock: 2
}

var productos = [producto1, producto2]

console.log('Tamaño del arreglo: ', productos.length);

for(var i = 0; i < productos.length; i++) {
  console.log(productos[i]);
  if(productos[i].stock <= 0) {
    console.log('¡YA NO HAY PRODUCTOS DE ' + productos[i].nombre + '!');
  }
}